\chapter{Implementace}
V této kapitole dojde k realizaci jednotlivých komponent systému podle definovaného návrhu v předchozí kapitole. Zahrnuta bude počáteční konfigurace a struktura projektu, která představí vybrané ukázky zdrojového kódu demonstrující klíčové aspekty implementace. Dále budou zmíněny nástroje, které byly využity pro vývoj aplikace a popis procesu jejího nasazení. Na závěr dojde k otestování implementovaného řešení.

\section{Konfigurace komponent}
Před implementací je nutné provést jisté konfigurační nastavení. Pro centralizaci obecné konfigurace aplikace je určen soubor \emph{application.yaml} v adresáři resources. Zde jsou nastaveny parametry pro připojení databáze Qdrant, propojení Ollama služby, ONNX modelu a další. Následně bude více představená konfigurace Google Drive cloudového úložiště a správa závislostí.

\subsection{Google Drive}
Pro integraci Google Drive s aplikací Spring Boot bylo postupováno následujícími kroky. Nejprve došlo k vytvoření projektu \textbf{GSight} v Google Cloud Console a povolení API Google Drive. Následně byl vytvořen servisní účet, který byl k tomuto projektu přiřazen. K účtu byl vytvořen klíč, pomocí něhož lze aplikaci propojit s úložištěm a využívat API služeb. Klíč \emph{gsight\_key.json} je uložen v adresáři resources/keys a je tak dostupný pro využití aplikací. 

\subsection{Správa závislostí}
Spring framework efektivně řeší správu závislostí prostřednictvím mechanismu \emph{dependency injection}, který je implementován pomocí \textbf{Bean}. Aby se předešlo vytváření redundantních instancí tříd, Spring většinou spravuje jedinou instanci každého Beanu jako singleton, který poté injektuje do ostatních komponent, kde je potřeba. Tento přístup odděluje konfiguraci od implementace kódu, což zvyšuje modularitu a usnadňuje testování. Bean mohou být konfigurovány deklarativně pomocí anotací, jako jsou \emph{@Component, @Service} a \emph{@Repository}, nebo programově ve speciálních konfiguračních třídách.

\input{text/code/google_config.tex}

Ukázka \ref{code:config} demonstruje konfiguraci úložiště Google Drive. Anotace \emph{@Configuration} indikuje, že se jedná o konfigurační třídu, ve které mohou existovat definice Bean. \emph{@Value} anotace přebírá parametr z konfiguračního souboru \emph{application.yaml}. Anotace \emph{@Bean} označuje, že metoda \emph{googleDriveService} vrací novou instanci třídy \emph{Drive}, kterou má Spring uchovat jako singleton pro inejkci závislostí. V případě, kdy z nějakého důvodu nemůže být instance třídy \emph{Drive} vytvořena, metoda vyvolá výjimku \emph{BeanCreationException}. Tato výjimka zamezí spuštění programu. 

V Adresáři config se dále nacházejí konfigurační třídy \emph{CollectionConfigManager, EmbeddingConfig, OllamaConfig} a \emph{QdrantConfig}.

\section{Struktura projektu}
Projekt byl inicializován pomocí nástroje Spring Initializr, který poskytl základní strukturu. Do této struktury byly postupně implementovány dílčí komponenty, čímž vzniklo funkční řešení.

\input{text/code/structure.tex}

\subsection{Interakce}
\label{sec:interaction}
Uživatel může s aplikací interagovat přes grafické rozhraní (GUI) nebo rozhraní REST.

REST API je realizováno skrze Spring framework, konkrétně pomocí anotace \emph{@RestController}, což umožňuje snadnou integraci HTTP požadavků a odpovědí modelu. Například, \emph{GoogleDocumentController} přijímá požadavky pro zpracování dokumentů. Při zaslání POST požadavku na koncový bod s URI \textbf{/api/document/process-data} metoda \emph{processData} spustí službu \emph{googleDocumentService} pro zpracování dat, a vrátí \emph{ResponseEntity} s výsledkem procesu.

\emph{OllamaChatController} nabízí koncové body pro interaktivní chat. Koncový bod \textbf{/api/ollama/chat} přijímá POST požadavek s dotazem ve formátu JSON, který je dále zpracováván. Pokud se k dotazu nenajde žádný kontext, vrátí se předdefinovaná odpověď \uv{I do not know the answer.}. V opačném případě se předá kontext s otázkou uživatele \emph{OllamaChatService}, která získá vygenerovanou odpověď z jazykového modelu. Tento controller rovněž podporuje streaming odpovědí skrze end\-point \textbf{/api/ollama/stream-chat} (viz~\ref{code:ollama_controller_stream}), který využívá media typ application/x-ndjson pro asynchronní posílání dat.

\input{text/code/ollama_controller_stream.tex}

Grafické rozhraní je implementováno s využitím komponent Vaadin frameworku. \emph{OllamaChat\-View} představuje hlavní objekt pro uživatelskou interakci, kde uživatelé mohou zadávat dotazy prostřednictvím textového pole a následně zobrazovat odpovědi v reálném čase. Komponenta \emph{MessageInput} slouží pro zadávání dotazů a je napojena na \emph{submitListener}, který reaguje na odeslané dotazy. Odpovědi jsou zobrazovány jako markdown zprávy, kde barevně rozlišené avatary reprezentují zprávy uživatele a systému. V případě chyby systém zobrazí chybovou zprávu a umožní uživateli pokračovat v interakci.

\input{text/code/ollama_view_listener}

V ukázce~\ref{code:ollama_view_listener} je znázorněna část akce \emph{submitListener}. Zde byla použita technologie reaktivního programování s využitím Project Reactor, což umožňuje asynchronní a neblokující zpracování dat. Díky odložené inicializaci pomocí \emph{Mono.defer} se aplikace vyhýbá zbytečnému zatížení a zahajuje zpracování dotazů až v momentě skutečné potřeby. V případě chyby nebo nedostatku dat je systém schopen uživateli okamžitě poskytnout relevantní zpětnou vazbu.

\input{text/code/ollama_view_metadata}

Metoda \emph{displayMetadata} v ukázce \ref{code:ollama_view_metadata} slouží k zobrazení metadat kontextu v uživatelském rozhraní aplikace. Funkce iteruje přes dostupná metadata, přičemž vytváří HTML elementy, které identifikují zdrojový soubor a poskytují přímý odkaz pro jeho zobrazení. Pro každý záznam je dynamicky vytvořen tučný text s názvem souboru následovaný odkazem, který umožňuje uživatelům zobrazit soubor v novém okně prohlížeče. Tento proces je zabalován do div kontejneru, který je nakonec vložen do markdown zprávy asistenta. Grafické rozhraní je zachyceno na obrázku~\ref{fig:view_screen}.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{images/view.png}
    \caption{Interakce uživatele přes grafické rozhraní}
    \label{fig:view_screen}
\end{figure}

\subsection{Služby}
\label{sec:services}
Vnitřní logika systému je implementována pomocí služeb, které komunikují s repozitářem nebo jinými službami.

Třída \emph{GoogleDocumentService} je specifická implementace abstraktní třídy \emph{DocumentService}, určená pro zpracování dokumentů uložených v Google Drive. V návrhu~\ref{sec:logic_layer} bylo představeno, že primární funkcionalitou třídy \emph{DocumentService} je metoda \emph{downloadAndProcessAll}, která umožňuje stahování a zpracování dokumentů podle jejich metadat. Metoda využívá dalších abstrtaktních metod, které právě třída \emph{GoogleDocumentService} implementuje.
% Tento proces začíná vytvořením nové kolekce dokumentů v repozitáři, následuje stahování souborů a jejich rozdělení na menší části (tokeny) pomocí TokenTextSplitter, a končí uložením zpracovaných dat zpět do repozitáře. 

\input{text/code/fetch_files.tex}

Metoda \emph{fetchFiles} z ukázky~\ref{code:fetch_files} zajišťuje výběr souborů z Google Drive dle specifikovaného ID složky. Toto ID je načteno z konfigurace \emph{application.yaml}. Soubory jsou iterativně získávány a konvertovány na interní datový typ \emph{FileDto}, který reprezentuje základní metadata o souboru včetně odkazů pro webové zobrazení a stahování jeho obsahu.

\input{text/code/download_file_content.tex}

Pro stahování obsahu souborů z Google Drive třída \emph{GoogleDocumentService} používá metodu \emph{downloadFileContent} z úkázky~\ref{code:download_file_content}, která na základě MIME typu souboru rozhoduje, zda bude soubor exportován nebo zda bude stažen přímo. Tento proces je nezbytný, neboť Google dokumenty \emph{docs, sheets} či \emph{slides} je nutné exportovat do ekvivalentního formátu Office Open XML. V případě neúspěchu operace je vyvolána výjimka \emph{FileIOException}, signalizující problém se stahováním souboru.

\input{text/code/retrieve_context.tex}

Dále \emph{GoogleDocumentService} poskytuje metody pro vyhledávání a získávání kontextu z dokumentů na základě textového dotazu. Metoda \emph{retrieveContextWithMetadata} (viz \ref{code:retrieve_context}) formuluje a následně odešle \emph{SearchRequest} dokumentovému repozitáři, který vrátí kontextové informace s metaday, které jsou poté uceleně předány volajícímu. Metoda \emph{retrieveContext} navrací kontext bez metadat.

\input{text/code/generate_stream_response.tex}

Služba \emph{OllamaChatService} využívá klienta \emph{OllamaChatClient} pro generování odpovědí na dotaz s přidruženým kontextem. Funkce \emph{generateResponse} synchronně zpracovává odpovědi metodou \emph{call} na \emph{ollamaChatClient}, zatímco \emph{generateStreamResponse} z ukázky~\ref{code:generate_stream_response} poskytuje odpovědi asynchronně ve formě streamu. V případě chyb v dostupnosti služby je vyvolána výjimka \emph{ChatServiceUnavailableException}. Pomocná funkce \emph{formPrompt} sestavuje \emph{Prompt} z kontextových informací a šablonových zpráv definovaných v \emph{PromptMessageHelper}, které řídí generativní model v reakcích na dotazy.

\subsection{Persistence}
Služba \emph{QdrantRepository} je konkrétní implementací abstraktní třídy \emph{DocumentRepository}. Slouží ke správě kolekcí dokumentů v prostředí vektorového úložiště Qdrant. Klíčovou funkcí této třídy je metoda \emph{recreateCollection} z ukázky~\ref{code:recreate_collection}, která se stará o obnovu nebo vytvoření nové kolekce dokumentů.

\input{text/code/recreate_collection.tex}

Proces obnovy kolekce zahrnuje kontrolu existence aktuální kolekce pomocí metody \emph{isCollectionExists}. Pokud kolekce neexistuje, inicializuje se nová kolekce voláním metody \emph{afterPropertiesSet} na objektu \emph{vectorStore}. V případě, že kolekce existuje, je nejprve asynchronně smazána a poté znovu inicializována. Tato operace může být komplikována možnými výjimkami, na které je reagováno vyvoláním \emph{ReCreateCollectionException} s popisem chyby.

\subsection{Řešení výjimek}
Výjimky vyvolané za běhu prostředí jsou zachyceny ve třídě \emph{RestResponseExceptionHandler}, která globálně odchytává výjimky v celé aplikaci v rámci využití REST API a dále je zpracovává. Ukázka~\ref{code:exception} demonstruje případ zachycení výjimky \emph{ChatServiceUnavailableException}. Metoda \emph{handleError} loguje výjimky a vrací \emph{ResponseEntity} s objektem \emph{ExceptionDto}, který obsahuje detaily o chybě.

\input{text/code/exception.tex}

\section{Nasazení}
Proces nasazení orchestrálně řídí Docker Compose, který spravuje jednotlivé kontejnery v síti \emph{gsight-backend}. Konfigurace tohoto procesu je specifikována v souboru \emph{compose.yaml}. Nasazení zahrnuje tři hlavní služby: gsight, qdrant a ollama, které odpovídají kontejnerům gsight-be, qdrant-db a ollama-ai. Inicializace začíná sestavením Docker obrazu pro službu gsight z příslušného Dockerfile. Před spuštěním kontejneru gsight-be je však nezbytné nejprve nasadit ostatní dva kontejnery. Služba qdrant se stará o spuštění vektorového úložiště Qdrant, které na portu 6333 přijímá komunikaci přes protokol gRPC a na portu 6334 prostřednictvím REST rozhraní. Služba ollama zase řídí načítání a správu generativních modelů. V rámci \emph{ollama\_entrypoint.sh} scri\-ptu dojde nejprve k odstranění signalizačního souboru, ke spuštění ollama serveru a k následné kontrole dostupnosti specifikovaného modelu. Pokud model chybí, je automaticky stažen. Po jeho úspěšném načtení se vytvoří signalizační soubor, který indikuje, že služba je připravena k obsluze požadavků přes REST rozhraní na portu 11434. Vzhledem k tomu, že Docker považuje službu za spuštěnou ihned po aktivaci ollama serveru, může dojít k zahájení aplikace gsight-be. Pokud by však nebyl model předem stažen, služba ollama by sice přijímala komunikaci, ale neměla by připravený model pro zpracování požadavků. Proto je ve scriptu \emph{gsight\_entrypoint.sh} zařazen mechanismus, který čeká na vytvoření signalizačního souboru \emph{ollama\_ready} ve sdíleném volume \emph{ollama-gsight-shared}. Jakmile jsou všechny služby úspěšně spuštěné, je aplikace gsight-be připravena k interakci buď přes grafické rozhraní nebo prostřednictvím REST API přes localhost na~portu 8080.

\section{Použité nástroje}
V rámci vývoje projektu byla využita řada specifických nástrojů, které značně usnadnily práci a zvýšily efektivitu vývoje. Tyto nástroje pokrývají široké spektrum úkolů od vývoje, přes verzování až po logování.

\begin{itemize}
    \item \mybold{IntelliJ IDEA} je vývojové prostředí (IDE) od společnosti JetBrains, které bylo použito pro celkový vývoj aplikace. Díky své komplexní podpoře jazyka Java a množství integrovaných nástrojů byla implementace snazší.
    \item \mybold{Lombok} byl užitečný nástroj pro zjednodušení vývoje tím, že redukoval potřebu psát opakující se části kódu. Speciálně anotace \emph{@RequiredArgsConstructor} automaticky generuje konstruktory s povinnými parametry, což eliminuje potřebu jejich ručního psaní. Anotace \emph{@Slf4j} byla použita k automatickému injektování objektu \emph{logger} v kombinaci s Logbabckem.
    \item \mybold{Logback} byl použit za účelem logování během vývoje a provozu aplikace. Jakým způsobem a v jakém formátu logy zaznamenávají je nastaveno v konfiguračním souboru \emph{logback-spring.xml}.
    \item \mybold{Gitlab} byl využit pro verzování zdrojových kódů projektu. Pro test sestavení byla vytvořena pipeline v souboru \emph{.gitlab-ci.yml}, která se spustí při každém commitu.
    \item \mybold{Maven} je nástroj pro řízení a automatizaci sestavování projektu. Maven byl využit k definování projektových závislostí, konfiguraci a sestavení aplikace. Jeho konfigurace se nachází v souboru \emph{pom.xml}.
\end{itemize}

% \section{Testování}
% Testování je zásadní součástí každého implementačního procesu. Často bývá opomíjeno, protože vyžaduje dodatečný čas a zdroje, které mohou být v krátkodobém horizontu vnímány jako neefektivní vynaložení úsilí. Nicméně, až po otestování všech možných scénářů a stavů, do kterých se aplikace může dostat, lze testované funkcionality s jistotou garantovat. Úspěšné testování slouží jako důkaz, že výsledné řešení splňuje garantované funkcionality.
