#!/bin/zsh

# Nastavení globálního vzoru pro vyhledávání v aktuálním adresáři a všech podadresářích
setopt extended_glob

find . -type f \( -name '*.aux'\
                    -o -name '*.bbl'\
                    -o -name '*.bcf'\
                    -o -name '*.blg'\
                    -o -name '*.fdb*'\
                    -o -name '*.fls'\
                    -o -name '*.lof'\
                    -o -name '*.log'\
                    -o -name '*.out'\
                    -o -name '*.run*'\
                    -o -name '*.synctex.gz'\
                    -o -name '*.tox'\
                    -o -name '*.xdv'\
                    -o -name '*.toc'\
                    -o -name '*.lot'\
                    -o -name '*.lol'\
                    -o -name '*-thesis.pdf'\
                     \) -exec rm {} +

rm -rf _minted*
