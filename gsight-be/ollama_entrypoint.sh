#!/bin/bash

model_name=${OLLAMA_MODEL}

rm /tmp/ollama_ready
echo "SERVING"
/bin/ollama serve &
serve_pid=$!

sleep 7

echo "CHECKING '${model_name}' ..."
if /bin/ollama list | grep -q "${model_name}"; then
    echo "'${model_name}' ALREADY EXISTS"
else
    echo "'${model_name}' NOT FOUND, PULLING ..."
    if /bin/ollama pull ${model_name}; then
        echo "PULL SUCCESSFUL"
    else
        echo "PULL FAILED"
        exit 1
    fi
fi

touch /tmp/ollama_ready
sleep 2
echo "RUNNING ${model_name} ..."
/bin/ollama run ${model_name}

echo "REATTACHING TO THE SERVER"

# Reattach to serve process
wait $serve_pid