# GSight
GSight is a Java application developed with Spring Boot and powered by Spring AI. Its primary objective is to deliver efficient solution for Retrieval Augmented Generation (RAG). GSight utilizes a dataset stored in Google Drive cloud storage. The application harnesses the intfloat/multilingual-e5-base model (based on xlm-roberta-base) for generating embeddings stored in the Qdrant vector database. The locally deployed Llama3 model is accessed through the Ollama service to enrich response generation.

![](videos/GSight.mp4)

## Boot Up
The application is divided into three services `gsight`, `qdrant`, and `ollama`. All of which can be launched using Docker. To start the application, run the command `docker compose up`. However, it's essential to ensure that specific requirements are met beforehand.

**System Requirements:**
- **Memory:** At least 16GB of RAM is recommended.

- **GPU:** (Optional) An NVIDIA GPU is recommended for the `ollama` service. If available, install the NVIDIA Container Toolkit [following these instructions](https://hub.docker.com/r/ollama/ollama). Uncomment `devices` section in `compose.yaml` file.

- **Docker:** Docker Compose version 2.23.1 or higher and Docker Engine 25.0.0 or higher must be installed.

If you encounter an error indicating insufficient memory, please refer to `compose.yaml` file for further understanding.

Once the application is running, access it through your web browser via http://localhost:8080.

## API documentation
http://localhost:8080/swagger-ui.html

## Google Drive
GSight is connected to Google Drive folder that can be accessed via [this link](https://drive.google.com/drive/u/1/folders/18auAJ9LU1_8k1t6B_yLQ5cxkqvG3zCX). The folder contains 20 documents with internal information about the fictitious company Faison. The documents are in PDF, DOCX, and Google Docs formats. 

## Demo
The following link servers as a demonstration of the GSight application: [GSight Demo](http://169.150.200.8:8080/)

## Question examples
- Vypiš mi v bodech, jaké mohu čerpat zaměstnanecké benefity.
- Shrň mi ve třech větách historický vývoj firmy Faison.
- Jak probíhá nábor nových zaměstnanců? Popiš mi jej stručně v bodech.
- Využívá Faison nějaké strategie budování týmu?
- Proč bych měl mít motivaci pracovat ve firmě Faison?
- Musím dodržovat nějaká pravidla nebo se držet určitých firemních zásad?
- Popiš mi pracovní politiky ve firmě. Ke každé uveď jednu větu.