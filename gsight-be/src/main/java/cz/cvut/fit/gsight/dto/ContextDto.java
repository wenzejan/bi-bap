package cz.cvut.fit.gsight.dto;

import java.util.List;
import java.util.Map;

public record ContextDto(
    String question,
    String context,
    List<Map<String, Object>> contextMetadata
){}
