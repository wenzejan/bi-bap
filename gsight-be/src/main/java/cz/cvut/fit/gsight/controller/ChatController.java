package cz.cvut.fit.gsight.controller;

import cz.cvut.fit.gsight.dto.RequestDto;
import cz.cvut.fit.gsight.dto.ResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Flux;

public interface ChatController {
    @Operation(
        summary = "Chat",
        description = "Generates answer based on user question."
    )
    ResponseEntity<ResponseDto> chat(@Valid RequestDto requestDto);

    @Operation(
        summary = "Chat as a stream",
        description = "Generates answer as a stream of ResponseDto based on user question."
    )
    Flux<ResponseDto> streamChat(@Valid RequestDto requestDto);
}
