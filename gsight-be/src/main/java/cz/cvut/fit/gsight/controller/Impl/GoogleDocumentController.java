package cz.cvut.fit.gsight.controller.Impl;

import cz.cvut.fit.gsight.controller.DocumentController;
import cz.cvut.fit.gsight.dto.ProcessResultDto;
import cz.cvut.fit.gsight.service.document.GoogleDocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/document")
public class GoogleDocumentController implements DocumentController {

    private final GoogleDocumentService googleDocumentService;

    @Override
    @PostMapping("/process-data")
    public ResponseEntity<ProcessResultDto> processData (){
        googleDocumentService.downloadAndProcessAll();
        return ResponseEntity.ok(new ProcessResultDto("Data were processed successfully"));
    }
}
