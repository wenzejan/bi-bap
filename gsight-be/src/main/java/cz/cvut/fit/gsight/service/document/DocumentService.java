package cz.cvut.fit.gsight.service.document;

import cz.cvut.fit.gsight.dto.ContextDto;
import cz.cvut.fit.gsight.dto.FileDto;
import cz.cvut.fit.gsight.repository.DocumentRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.document.Document;
import org.springframework.ai.reader.tika.TikaDocumentReader;
import org.springframework.ai.transformer.splitter.TextSplitter;
import org.springframework.core.io.Resource;

import java.util.List;

/**
 * Provides an abstract framework for processing documents fetched from a remote source,
 * supporting operations such as download, process, and contextual retrieval based on metadata.
 * This class defines core workflow methods and abstract methods that need to be implemented
 * by subclasses specific to different data sources and repositories.
 *
 * <p>It initializes the document processing if the collection is empty at post-construction phase
 * and provides methods to download and process all documents, retrieve context with metadata,
 * and manage document metadata.</p>
 *
 * @param <Repository> The type of document repository used for storing and retrieving documents.
 */
@RequiredArgsConstructor
public abstract class DocumentService<Repository extends DocumentRepository<?>> {
    protected final Repository documentRepository;
    protected final TextSplitter textSplitter;

    /**
     * Downloads and processes all documents from a remote source.
     * This method triggers a complete refresh of the document collection in the repository.
     */
    public void downloadAndProcessAll() {
        documentRepository.recreateCollection();
        List<FileDto> files = fetchFiles();

        for (FileDto file : files) {
            TikaDocumentReader tikaDocumentReader = new TikaDocumentReader(downloadFileContent(file));
            List<Document> docs = tikaDocumentReader.get();
            metadataSet(docs.getFirst(), file);
            documentRepository.add(textSplitter.apply(docs));
        }
    }

    /**
     * Initializes document processing if the document collection is found to be empty.
     * This method is called post-construction.
     */
    @PostConstruct
    protected void init (){
        if (documentRepository.isCollectionEmpty())
            downloadAndProcessAll();
    }

    /**
     * Retrieves a context enriched with metadata for a given query.
     * @param question The query for which the context is to be retrieved.
     * @return A ContextDto containing relevant data and metadata based on the question.
     */
    public abstract ContextDto retrieveContextWithMetadata (String question);

    /**
     * Retrieves a textual context for a given query.
     * @param question The query for which the context is to be retrieved.
     * @return A string representing the context related to the question.
     */
    public abstract String retrieveContext(String question);

    /**
     * Fetches a list of FileDto with metadata from a remote source.
     * @return A list of FileDto objects representing the files to be processed.
     */
    protected abstract List<FileDto> fetchFiles();

    /**
     * Downloads the content of a file identified by a FileDto.
     * @param file The FileDto containing the details needed to download the content.
     * @return A Resource representing the downloaded content of the file.
     */
    protected abstract Resource downloadFileContent(FileDto file);

    /**
     * Sets metadata for a document based on the associated file metadata.
     * @param document The document to which metadata is to be set.
     * @param file The FileDto containing metadata information.
     */
    private void metadataSet(Document document, FileDto file) {
        document.getMetadata().putAll(FileDto.getFileMetadata(file));
    }
}
