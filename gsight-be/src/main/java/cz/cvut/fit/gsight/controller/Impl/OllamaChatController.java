package cz.cvut.fit.gsight.controller.Impl;

import cz.cvut.fit.gsight.controller.ChatController;
import cz.cvut.fit.gsight.dto.ContextDto;
import cz.cvut.fit.gsight.dto.RequestDto;
import cz.cvut.fit.gsight.dto.ResponseDto;
import cz.cvut.fit.gsight.service.chat.OllamaChatService;
import cz.cvut.fit.gsight.service.document.GoogleDocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RequestMapping("/api/ollama")
@RestController
@RequiredArgsConstructor
public class OllamaChatController implements ChatController {
    private final OllamaChatService ollamaChatService;

    private final GoogleDocumentService googleDocumentService;

    @Override
    @PostMapping("/chat")
    public ResponseEntity<ResponseDto> chat (@RequestBody RequestDto requestDto){
        ContextDto context = googleDocumentService.retrieveContextWithMetadata(requestDto.question());
        return ResponseEntity.ok(
            context
                .context()
                .isEmpty()
                ? ResponseDto.emptyContextResponse()
                : ResponseDto.fromChatResponse(ollamaChatService.generateResponse(context), context));
    }

    @Override
    @PostMapping(value = "/stream-chat", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<ResponseDto> streamChat(@RequestBody RequestDto requestDto) {
        ContextDto context = formContextWithNullableMetadata(requestDto.question());
        return context
                .context()
                .isEmpty()
                ? ResponseDto.emptyContextFluxResponse()
                : ResponseDto.fromChatStream(ollamaChatService.generateStreamResponse(context));
    }

    private ContextDto formContextWithNullableMetadata (String question){
        return new ContextDto(
            question,
            googleDocumentService.retrieveContext(question),
            null);
    }
}
//    @Deprecated
//    @PostMapping("/stream-chat-test")
//    public Flux<ChatResponse> streamChatTest(@RequestBody RequestDto requestDto) {
//        return ollamaChatService.generateStreamResponse(formContextWithNullableMetadata(requestDto.question()));
//    }
