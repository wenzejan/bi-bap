package cz.cvut.fit.gsight.service.chat;

import cz.cvut.fit.gsight.dto.ContextDto;
import org.springframework.ai.chat.ChatResponse;
import reactor.core.publisher.Flux;

/**
 * Defines the contract for a chat service capable of generating chat responses based on provided context.
 * This interface provides methods for both synchronous and asynchronous response generation,
 * facilitating interaction in a conversational model.
 *
 * <p>Methods included:
 * - {@link #generateResponse(ContextDto)} for generating a synchronous response based on the context.
 * - {@link #generateStreamResponse(ContextDto)} for generating a stream of responses asynchronously,
 *   suitable for handling continuous or multi-turn interactions.</p>
 *
 * @see ContextDto
 * @see ChatResponse
 */
public interface ChatService {

    /**
     * Generates a single synchronous response for a given context.
     *
     * @param document The context information based on which the chat response is formulated.
     * @return A {@link ChatResponse} encapsulating the result of the chat interaction.
     */
    ChatResponse generateResponse(ContextDto document);

    /**
     * Generates a stream of responses asynchronously for a given context.
     *
     * @param document The context information based on which the chat responses are streamed.
     * @return A {@link reactor.core.publisher.Flux<ChatResponse>} of chat responses, providing an ongoing interaction.
     */
    Flux<ChatResponse> generateStreamResponse(ContextDto document);
}
