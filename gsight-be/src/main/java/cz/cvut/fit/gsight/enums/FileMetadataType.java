package cz.cvut.fit.gsight.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FileMetadataType {
    SOURCE ("source"),
    MIME_TYPE("mime_type"),
    DOWNLOAD_LINK("download_url"),
    VIEW_LINK("view_url");

    private final String value;
}
