package cz.cvut.fit.gsight.repository;

import cz.cvut.fit.gsight.config.CollectionConfigManager;
import cz.cvut.fit.gsight.exception.DbConnectionException;
import cz.cvut.fit.gsight.exception.ReCreateCollectionException;
import io.qdrant.client.QdrantClient;
import org.springframework.ai.vectorstore.qdrant.QdrantVectorStore;
import org.springframework.stereotype.Repository;

@Repository
public class QdrantRepository extends DocumentRepository<QdrantVectorStore> {
    private final QdrantClient qdrantClient;

    public QdrantRepository(QdrantVectorStore vectorStore, QdrantClient qdrantClient) {
        super(vectorStore);
        this.qdrantClient = qdrantClient;
    }

    @Override
    public void recreateCollection() {
        try {
            if (!isCollectionExists()) {
                vectorStore.afterPropertiesSet();
            }

            else {
                qdrantClient.deleteCollectionAsync(CollectionConfigManager.getCollectionName()).get();
                vectorStore.afterPropertiesSet();
            }
        }

        catch (Exception ex) {
            throw new ReCreateCollectionException("Failed to recreate the collection due to an unexpected error", ex);
        }
    }

    @Override
    protected boolean isCollectionExists() {
        try {
            return this.qdrantClient.listCollectionsAsync().get().stream().anyMatch(c -> c.equals(CollectionConfigManager.getCollectionName()));
        } catch (Exception ex) {
            throw new DbConnectionException("Failed to check if collection exists due to a connection error", ex);
        }
    }

    @Override
    public boolean isCollectionEmpty() {
        try {
            return this.qdrantClient.getCollectionInfoAsync(CollectionConfigManager.getCollectionName()).get().getPointsCount() == 0;
        } catch (Exception ex) {
            throw new DbConnectionException("Failed to check if collection is empty due to a connection error", ex);
        }
    }
}
