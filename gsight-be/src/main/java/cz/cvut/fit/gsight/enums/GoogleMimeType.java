package cz.cvut.fit.gsight.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum GoogleMimeType {
    APPLICATION_VND_GOOGLE_APPS_DOCUMENT ("application/vnd.google-apps.document"),
    APPLICATION_VND_GOOGLE_APPS_SPREADSHEET ("application/vnd.google-apps.spreadsheet"),
    APPLICATION_VND_GOOGLE_APPS_PRESENTATION ("application/vnd.google-apps.presentation");

    private final String value;
}
