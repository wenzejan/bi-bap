package cz.cvut.fit.gsight.exception;

public class FileIOException extends RuntimeException {
    public FileIOException(String message, Throwable ex) {
        super(message, ex);
    }
    public FileIOException(String message) {
        super(message);
    }
}

