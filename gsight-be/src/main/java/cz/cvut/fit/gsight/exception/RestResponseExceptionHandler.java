package cz.cvut.fit.gsight.exception;

import cz.cvut.fit.gsight.dto.ExceptionDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class RestResponseExceptionHandler {

    @ExceptionHandler(ChatServiceUnavailableException.class)
    protected ResponseEntity<ExceptionDto> handleChatServiceUnavailableException(ChatServiceUnavailableException ex) {
        return handleError(ex.getMessage(), ex, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(DbConnectionException.class)
    protected ResponseEntity<ExceptionDto> handleDbConnectionException(DbConnectionException ex) {
        return handleError(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(FileIOException.class)
    protected ResponseEntity<ExceptionDto> handleFileIOException(FileIOException ex) {
        return handleError(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ReCreateCollectionException.class)
    protected ResponseEntity<ExceptionDto> handleReCreateCollectionException(ReCreateCollectionException ex) {
        return handleError(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<ExceptionDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        String errorMessage = ex.getBindingResult().getFieldErrors().stream()
            .map(DefaultMessageSourceResolvable::getDefaultMessage)
            .collect(Collectors.joining(", "));
        return handleError("Validation error: " + errorMessage, ex, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ExceptionDto> handleError(String message, Throwable ex, HttpStatus status) {
        logger.error(message, ex);
        return new ResponseEntity<>(ExceptionDto.from(status.value(), message), status);
    }
}
