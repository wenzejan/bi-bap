package cz.cvut.fit.gsight.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;

@Schema(description = "Carries a user's question for processing.")
public record RequestDto (
    @NotEmpty
    @Schema(description = "The question posed by the user.")
    String question) {

    public static RequestDto from (String question){
        return new RequestDto(question);
    }
}
