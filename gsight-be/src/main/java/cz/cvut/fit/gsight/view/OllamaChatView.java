package cz.cvut.fit.gsight.view;

import com.vaadin.flow.component.messages.MessageInput;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.gsight.dto.ContextDto;
import cz.cvut.fit.gsight.dto.ResponseDto;
import cz.cvut.fit.gsight.enums.FileMetadataType;
import cz.cvut.fit.gsight.service.chat.OllamaChatService;
import cz.cvut.fit.gsight.service.document.GoogleDocumentService;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.vaadin.firitin.components.messagelist.MarkdownMessage;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.Serial;
import java.util.Optional;

@Slf4j
@Route("")
public class OllamaChatView extends VerticalLayout {
    @Serial
    private static final long serialVersionUID = 8734612770125105826L;
    private static final String USER_AVATAR_COLOR = "#aa9a15";
    private static final String SYSTEM_AVATAR_COLOR = "#c72e16";
    private static final String SYSTEM_ERROR_MESSAGE = "[Error] Unable to retrieve response: ";

    public OllamaChatView(OllamaChatService ollamaChatService, GoogleDocumentService googleDocumentService) {
        var messageList = new VerticalLayout();
        var messageInput = new MessageInput();

        messageInput.setWidthFull();
        messageInput.addSubmitListener(event -> submitPromptListener(event, ollamaChatService, googleDocumentService, messageList));
        setSizeFull();
        addClassName("centered-content");
        add(messageList, messageInput);
    }

    private static void submitPromptListener(
        MessageInput.SubmitEvent event,
        OllamaChatService ollamaChatService,
        GoogleDocumentService googleDocumentService,
        VerticalLayout messageList) {

        var question = event.getValue();
        var userMessage = new MarkdownMessage(question, "You", new MarkdownMessage.Color(USER_AVATAR_COLOR));
        var assistantMessage = new MarkdownMessage("Assistant", MarkdownMessage.Color.AVATAR_PRESETS[6]);
        var errorMessage = new MarkdownMessage("System", new MarkdownMessage.Color(SYSTEM_AVATAR_COLOR));
        messageList.add(userMessage, assistantMessage);

        Mono.defer(() -> Mono.just(googleDocumentService.retrieveContextWithMetadata(question)))
            .subscribeOn(Schedulers.boundedElastic())
            .flatMap(context -> {
                if (context.context().isEmpty()) {
                    assistantMessage.appendMarkdownAsync(ResponseDto.emptyContextResponse().getResponse());
                    return Mono.empty();
                }

                else {
                    return ollamaChatService.generateStreamResponse(context)
                        .map(res -> ResponseDto.fromChatResponse(res).getResponse())
                        .doOnNext(assistantMessage::appendMarkdownAsync)
                        .last()
                        .flatMap(last -> Mono.fromRunnable(() -> displayMetadata(context, assistantMessage)));
                }
            })
            .doOnError(ex -> {
                logger.error(ex.getMessage(), ex);
                messageList.remove(assistantMessage);
                messageList.add(errorMessage);
                errorMessage.appendMarkdownAsync(SYSTEM_ERROR_MESSAGE + ex.getMessage());
            })
            .onErrorResume(e -> Mono.empty())
            .subscribe();
    }

    private static void displayMetadata(ContextDto context, MarkdownMessage assistantMessage ) {
        Element main = new Element(Tag.valueOf("div"), "");
        context.contextMetadata()
            .forEach(metadata -> {
                main.appendElement("b").text("Source file: ");

                String source = Optional.ofNullable(metadata.get(FileMetadataType.SOURCE.getValue())).orElse("").toString();
                main.appendText(source + " ");

                String viewLink = Optional.ofNullable(metadata.get(FileMetadataType.VIEW_LINK.getValue())).orElse("").toString();
                main.appendElement("a")
                    .text("(view)")
                    .attr("href", viewLink)
                    .attr("target", "_blank");

                main.appendChild(new Element(Tag.valueOf("br"), ""));
            });
            assistantMessage.appendMarkdownAsync(main.outerHtml());
    }
}
