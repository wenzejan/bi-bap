package cz.cvut.fit.gsight.dto;

import com.google.api.services.drive.model.File;
import cz.cvut.fit.gsight.enums.FileMetadataType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;
import java.util.Optional;

@Data
@AllArgsConstructor
public class FileDto {
    private String id;
    private String name;
    private String mimeType;
    private String webContentLink;
    private String viewContentLink;

    public static FileDto fromGoogleFile(File file){
        return new FileDto(
            file.getId(),
            file.getName(),
            file.getMimeType(),
            file.getWebContentLink(),
            file.getWebViewLink()
        );
    }

    public static Map<String, String> getFileMetadata(FileDto fileDto){
        return Map.of(
            FileMetadataType.SOURCE.getValue(), Optional.ofNullable(fileDto.getName()).orElse(""),
            FileMetadataType.MIME_TYPE.getValue(), Optional.ofNullable(fileDto.getMimeType()).orElse(""),
            FileMetadataType.DOWNLOAD_LINK.getValue(), Optional.ofNullable(fileDto.getWebContentLink()).orElse(""),
            FileMetadataType.VIEW_LINK.getValue(), Optional.ofNullable(fileDto.getViewContentLink()).orElse("")
        );
    }
}
