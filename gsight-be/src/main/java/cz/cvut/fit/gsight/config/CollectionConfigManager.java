package cz.cvut.fit.gsight.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
public class CollectionConfigManager {
    private static String collectionName;

    public static void setCollectionName(String collectionName) {
        CollectionConfigManager.collectionName = collectionName;
    }

    public static String getCollectionName() {
        return CollectionConfigManager.collectionName;
    }
}
