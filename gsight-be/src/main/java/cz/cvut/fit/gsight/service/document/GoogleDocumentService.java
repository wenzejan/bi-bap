package cz.cvut.fit.gsight.service.document;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import cz.cvut.fit.gsight.dto.ContextDto;
import cz.cvut.fit.gsight.dto.FileDto;
import cz.cvut.fit.gsight.exception.FileIOException;
import cz.cvut.fit.gsight.repository.QdrantRepository;
import org.springframework.ai.document.Document;
import org.springframework.ai.transformer.splitter.TokenTextSplitter;
import org.springframework.ai.vectorstore.SearchRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GoogleDocumentService extends DocumentService<QdrantRepository> {

    private final Drive googleDriveService;

    @Value("${google.drive.folder-id}")
    private String folderId;

    @Value("${spring.ai.vectorstore.qdrant.similarity-threshold}")
    private Double similarityThreshold;

    public GoogleDocumentService(QdrantRepository documentRepository, Drive googleDriveService, TokenTextSplitter tokenTextSplitter) {
        super(documentRepository, tokenTextSplitter);
        this.googleDriveService = googleDriveService;
    }

    @Override
    public ContextDto retrieveContextWithMetadata(String question) {
        SearchRequest request = SearchRequest.query(question).withSimilarityThreshold(similarityThreshold);
        var documents = documentRepository.similaritySearch(request);
        return new ContextDto(
            question,
            formContext(documents),
            documents
                .stream()
                .map(Document::getMetadata)
                .peek(metadata -> metadata.remove("distance"))
                .distinct()
                .toList()
        );
    }

    // Form request here
    @Override
    public String retrieveContext(String question) {
        SearchRequest request = SearchRequest.query(question).withSimilarityThreshold(similarityThreshold);
        return formContext(documentRepository.similaritySearch(request));
    }

    // Rozšíření do budoucna:
    // Metadata o souborech uchovávat v databázi spolu s IDs jednotlivých chunků.
    // Tahat si o souborech i datum poslední modifikace a na základě toho dokumenty přeindexovat.
    @Override
    public List<FileDto> fetchFiles() {
        List<FileDto> fetchedFiles = new ArrayList<>();

        try {
            String pageToken = null;
            do {
                FileList result = googleDriveService.files().list()
                    .setQ("'" + folderId + "' in parents")
                    .setPageSize(10)
                    .setFields("nextPageToken, files(id, name, mimeType, webContentLink, webViewLink)")
                    .setPageToken(pageToken)
                    .execute();

                List<File> googleFiles = result.getFiles();
                fetchedFiles.addAll(googleFiles.stream().map(FileDto::fromGoogleFile).toList());
                pageToken = result.getNextPageToken();
            }
            while (pageToken != null);
        }

        catch (IOException ex) {
            throw new FileIOException("Error occurred during files fetching", ex);
        }

        return fetchedFiles;
    }

    @Override
    public ByteArrayResource downloadFileContent(FileDto file) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            Optional<String> fileMimeType = ExportFormatMapper.getExportFormat(file.getMimeType());

            if (fileMimeType.isPresent()) {
                googleDriveService.files().export(file.getId(), fileMimeType.get()).executeMediaAndDownloadTo(baos);
            } else {
                googleDriveService.files().get(file.getId()).executeMediaAndDownloadTo(baos);
            }

            return new ByteArrayResource(baos.toByteArray());
        }

        catch (IOException ex) {
            throw new FileIOException(String.format("File %s could not be downloaded", file.getName()), ex);
        }
    }

    private String formContext(List<Document> documents) {
        return documents.stream().map(Document::getContent)
            .map(content -> content.replaceAll("\\s+", " "))
            .collect(Collectors.joining(" "));
    }
}
