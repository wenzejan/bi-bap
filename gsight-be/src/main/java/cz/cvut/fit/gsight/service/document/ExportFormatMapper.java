package cz.cvut.fit.gsight.service.document;

import com.google.common.net.MediaType;
import cz.cvut.fit.gsight.enums.GoogleMimeType;

import java.util.Map;
import java.util.HashMap;
import java.util.Optional;

public class ExportFormatMapper {
    private static final Map<String, String> MIME_TYPE_TO_EXPORT_FORMAT;

    static {
        MIME_TYPE_TO_EXPORT_FORMAT = new HashMap<>();
        MIME_TYPE_TO_EXPORT_FORMAT.put(GoogleMimeType.APPLICATION_VND_GOOGLE_APPS_DOCUMENT.getValue(), MediaType.OOXML_DOCUMENT.toString());
        MIME_TYPE_TO_EXPORT_FORMAT.put(GoogleMimeType.APPLICATION_VND_GOOGLE_APPS_SPREADSHEET.getValue(), MediaType.OOXML_SHEET.toString());
        MIME_TYPE_TO_EXPORT_FORMAT.put(GoogleMimeType.APPLICATION_VND_GOOGLE_APPS_PRESENTATION.getValue(), MediaType.OOXML_PRESENTATION.toString());
    }

    public static Optional<String> getExportFormat(String mimeType) {
        return Optional.ofNullable(MIME_TYPE_TO_EXPORT_FORMAT.getOrDefault(mimeType, null));
    }
}
