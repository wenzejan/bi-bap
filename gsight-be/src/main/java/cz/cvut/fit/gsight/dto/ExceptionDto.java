package cz.cvut.fit.gsight.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Schema(description = "Encapsulates error details for application exceptions.")
public record ExceptionDto(

    @NotNull
    @Schema(description = "The HTTP status code associated with the error.")
    Integer status,

    @NotBlank
    @Schema(description = "The error message describing the exception.")
    String message) {

    public static ExceptionDto from(Integer status, String message) {
        return new ExceptionDto(status, message);
    }
}
