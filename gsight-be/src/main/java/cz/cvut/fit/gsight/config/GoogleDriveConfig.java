package cz.cvut.fit.gsight.config;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.util.Collections;

@Configuration
public class GoogleDriveConfig {

    @Value("${google.drive.key-path}")
    private Resource keyResource;

    @Value("${spring.application.name}")
    private String applicationName;

    @Bean
    public Drive googleDriveService() {
        try {
            GoogleCredentials credentials = GoogleCredentials.fromStream(keyResource.getInputStream())
                .createScoped(Collections.singletonList(DriveScopes.DRIVE));

            return
                new Drive.Builder
                    (
                        GoogleNetHttpTransport.newTrustedTransport(),
                        GsonFactory.getDefaultInstance(),
                        new HttpCredentialsAdapter(credentials)
                    )
                    .setApplicationName(applicationName)
                    .build();
        } catch (Exception ex) {
            throw new BeanCreationException("googleDriveService", "Unable to create Google Drive Service", ex);
        }
    }
}