package cz.cvut.fit.gsight.exception;

public class ReCreateCollectionException extends RuntimeException {
    public ReCreateCollectionException(String message, Throwable ex) {
        super(message, ex);
    }
    public ReCreateCollectionException(String message) {
        super(message);
    }
}
