package cz.cvut.fit.gsight.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.ai.document.Document;
import org.springframework.ai.vectorstore.SearchRequest;
import org.springframework.ai.vectorstore.VectorStore;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Provides a generic framework for managing documents within a vector storage environment,
 * offering core functionality such as adding, deleting, and searching documents based on their
 * vector representations. This class is designed to be extended by specific implementations
 * that tailor the interaction with different vector storage technologies.
 *
 * <p>This abstract class defines both concrete and abstract methods:
 * - Concrete methods provide common functionalities that are generally applicable across
 *   different vector stores, such as performing similarity searches or deleting documents by IDs.
 * - Abstract methods must be implemented by subclasses to handle specific behaviors such as
 *   checking if the collection is empty, recreating collections, and verifying the existence of collections.</p>
 *
 * @param <Store> The type of vector store used by the repository to handle document operations.
 */
@RequiredArgsConstructor
public abstract class DocumentRepository<Store extends VectorStore>{
    protected final Store vectorStore;

    /**
     * Adds a list of documents to the vector store.
     * @param documents The list of documents to be added to the store.
     */
    public void add(List<Document> documents) {
        vectorStore.add(documents);
    }

    /**
     * Attempts to delete documents identified by their IDs from the vector store.
     * @param documentIds The list of document IDs to delete.
     * @return An Optional containing a Boolean value indicating success or failure.
     */
    public Optional<Boolean> deleteByIds(List<String> documentIds) {
        return vectorStore.delete(documentIds);
    }

    /**
     * Performs a similarity search in the vector store based on the provided search request.
     * @param request The search request containing search parameters.
     * @return A list of documents that match the search criteria; returns an empty list if the collection does not exist.
     */
    public List<Document> similaritySearch(SearchRequest request) {
        if (isCollectionExists())
            return vectorStore.similaritySearch(request);
        return Collections.emptyList();
    }

    /**
     * Recreates the document collection, necessary for resetting or updating the collection structure.
     */
    public abstract void recreateCollection ();

    /**
     * Checks whether the document collection in the store is empty.
     * @return true if the collection is empty, false otherwise.
     */
    public abstract boolean isCollectionEmpty ();

    /**
     * Verifies the existence of the default document collection in the vector store.
     * @return true if the collection exists, false otherwise.
     */
    protected abstract boolean isCollectionExists();
}
