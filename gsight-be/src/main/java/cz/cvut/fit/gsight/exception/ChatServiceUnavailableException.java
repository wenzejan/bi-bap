package cz.cvut.fit.gsight.exception;

public class ChatServiceUnavailableException extends RuntimeException {
    public ChatServiceUnavailableException(String message, Throwable ex) {
        super(message, ex);
    }

    public ChatServiceUnavailableException(String message) {
        super(message);
    }
}