package cz.cvut.fit.gsight.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "Represents the outcome of a document processing operation.")
public record ProcessResultDto (
    @NotBlank
    @Schema(description = "Summary of the document processing result, indicating success or errors.")
    String result) {}