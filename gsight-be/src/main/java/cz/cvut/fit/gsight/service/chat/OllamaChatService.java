package cz.cvut.fit.gsight.service.chat;


import cz.cvut.fit.gsight.dto.ContextDto;
import cz.cvut.fit.gsight.exception.ChatServiceUnavailableException;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.ollama.OllamaChatClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OllamaChatService implements ChatService {

    private final OllamaChatClient ollamaChatClient;

    private static final String OLLAMA_ERROR_MESSAGE = "Ollama chat service unavailable";

    @Override
    public ChatResponse generateResponse(ContextDto document) {
        try {
            return ollamaChatClient.call(formPrompt(document));
        }
        catch (ResourceAccessException ex) {
            throw new ChatServiceUnavailableException(OLLAMA_ERROR_MESSAGE, ex);
        }
    }

    @Override
    public Flux<ChatResponse> generateStreamResponse(ContextDto document) {
        return ollamaChatClient.stream(formPrompt(document))
            .onErrorMap(ex -> new ChatServiceUnavailableException(OLLAMA_ERROR_MESSAGE, ex));
    }

    private Prompt formPrompt(ContextDto document) {
        return new Prompt(
            List.of(
                PromptMessageHelper.buildSystemMessage(),
                PromptMessageHelper.buildUserMessage(document)));
    }
}
