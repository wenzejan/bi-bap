package cz.cvut.fit.gsight.exception;

public class DbConnectionException extends RuntimeException {
    public DbConnectionException(String message, Throwable ex) {
        super(message, ex);
    }

    public DbConnectionException(String message) {
        super(message);
    }
}