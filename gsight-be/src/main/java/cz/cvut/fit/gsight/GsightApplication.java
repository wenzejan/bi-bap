package cz.cvut.fit.gsight;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Push
@Theme(value = "gsight", variant = Lumo.DARK)
@SpringBootApplication
public class GsightApplication implements AppShellConfigurator {

    public static void main(String[] args) {
        SpringApplication.run(GsightApplication.class, args);
    }
}
