package cz.cvut.fit.gsight.config;

import org.springframework.ai.ollama.OllamaChatClient;
import org.springframework.ai.ollama.api.OllamaApi;
import org.springframework.ai.ollama.api.OllamaOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class OllamaConfig {

    @Value("${spring.ai.ollama.chat.options.model}")
    private String model;

    @Value("${spring.ai.ollama.host}")
    private String host;

    @Value("${spring.ai.ollama.port}")
    private String port;

    @Value("${spring.ai.ollama.chat.options.temperature}")
    private Float temperature;

    private static final String PROTOCOL_SCHEME = "http";

    @Bean
    public OllamaApi ollamaApi() {
        return new OllamaApi(UriComponentsBuilder.newInstance()
            .scheme(PROTOCOL_SCHEME)
            .host(host)
            .port(port)
            .toUriString());
    }

    @Bean
    public OllamaChatClient ollamaChatClient(OllamaApi ollamaApi) {

        return new OllamaChatClient(ollamaApi)
            .withDefaultOptions(OllamaOptions.create()
                .withModel(model)
                .withTemperature(temperature));
    }
}
