package cz.cvut.fit.gsight.controller;

import cz.cvut.fit.gsight.dto.ProcessResultDto;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;

public interface DocumentController {

    @Operation(
        summary = "Documents preprocessing",
        description = "Downloads and preprocess the data from the specified cloud storage for information retrieval"
    )
    ResponseEntity<ProcessResultDto> processData();
}
