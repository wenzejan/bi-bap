package cz.cvut.fit.gsight.config;

import io.qdrant.client.QdrantClient;
import io.qdrant.client.QdrantGrpcClient;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.ai.embedding.EmbeddingClient;
import org.springframework.ai.vectorstore.qdrant.QdrantVectorStore;
import org.springframework.ai.vectorstore.qdrant.QdrantVectorStore.QdrantVectorStoreConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(value = "spring.ai.vectorstore.qdrant")
public class QdrantConfig {

    private String collectionName;

    private String host;

    private int port;

    private static final boolean USE_TLS = false;

    @PostConstruct
    public void init() {
        CollectionConfigManager.setCollectionName(collectionName);
    }

    @Bean
    public QdrantClient qdrantClient() {
        return new QdrantClient(QdrantGrpcClient.newBuilder(host, port, USE_TLS) .build());
    }

    @Bean
    public QdrantVectorStoreConfig qdrantVectorStoreConfig() {

        return QdrantVectorStoreConfig.builder()
            .withCollectionName(collectionName)
            .withHost(host)
            .withPort(port)
            .build();
    }

    @Bean
    public QdrantVectorStore qdrantVectorStore(QdrantVectorStoreConfig config, EmbeddingClient embeddingClient) {
        return new QdrantVectorStore(config, embeddingClient);
    }
}
