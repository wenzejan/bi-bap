package cz.cvut.fit.gsight.service.chat;

import cz.cvut.fit.gsight.dto.ContextDto;
import org.springframework.ai.chat.messages.SystemMessage;
import org.springframework.ai.chat.messages.UserMessage;

public class PromptMessageHelper {
    private static final String SYSTEM_MESSAGE_DEFAULT = "Answer the question in the language of the question based on the context below. Keep the answer short and concise. "
        + "Respond \"Unsure about answer\" if not sure about the answer.";
    private static final String SYSTEM_MESSAGE_ENHANCED = "You are a helpful AI support agent who helps employees in a company \"Faison\" by following directives and answering questions."
        + "Generate your response by following the steps:"
        + "1. For given question select the most relevant information from the given context."
        + "2. Answer the question in the same language as is the question."
        + "3. Translate your answers to the Czech language if you are not sure about the language of the question."
        + "4. Do not answer the question if the answer would not be based on given context!"
        + "5. Answer \"I do not know the answer\" if the information can not be retrieved from the context."
        + "6. If the knowledge of your answer is not within the context then you must not say it!"
        + "7. Now only show your final response! Do not provide any explanations or details.";

    public static final String EMPTY_CONTEXT_RESPONSE = "I do not know the answer.";

    public static SystemMessage buildSystemMessage(){
        return new SystemMessage(SYSTEM_MESSAGE_ENHANCED);
    }

    public static UserMessage buildUserMessage(ContextDto document){
        return new UserMessage("Context" + document.context() + "\n" + "Question: " + document.question() + "\n" + "Answer: ");
    }
}
