package cz.cvut.fit.gsight.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.ai.embedding.EmbeddingClient;
import org.springframework.ai.transformer.splitter.TokenTextSplitter;
import org.springframework.ai.transformers.TransformersEmbeddingClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties("spring.ai.embedding.transformer.onnx")
public class EmbeddingConfig {

    private String modelUri;

    private String tokenizerUri;

    private static final int SPLITTER_DEFAULT_CHUNK_SIZE = 512;
    private static final int SPLITTER_MIN_CHUNK_SIZE_CHARS = 350;
    private static final int SPLITTER_MIN_CHUNK_LENGTH_TO_EMBED = 5;
    private static final int SPLITTER_MAX_NUM_CHUNKS = 10000;
    private static final boolean SPLITTER_KEEP_SEPARATOR = true;

    @Bean
    public TokenTextSplitter tokenTextSplitter() {
        return new TokenTextSplitter(
            SPLITTER_DEFAULT_CHUNK_SIZE,
            SPLITTER_MIN_CHUNK_SIZE_CHARS,
            SPLITTER_MIN_CHUNK_LENGTH_TO_EMBED,
            SPLITTER_MAX_NUM_CHUNKS,
            SPLITTER_KEEP_SEPARATOR
        );
    }

    @Bean
    public EmbeddingClient transformersEmbeddingClient() throws Exception {
        TransformersEmbeddingClient embeddingClient = new TransformersEmbeddingClient();
        embeddingClient.setTokenizerResource(tokenizerUri);
        embeddingClient.setModelResource(modelUri);
        embeddingClient.afterPropertiesSet();
        return embeddingClient;
    }
}
