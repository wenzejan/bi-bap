package cz.cvut.fit.gsight.it.ollamachat;

import org.junit.jupiter.api.Nested;

import java.util.List;

public class TestContextHelper {

    @Nested
    class Requirement{
        public static final List<String> REQ_BENEFITS = List.of(
            "Maximální využití kariérního poradenství",
            "Typy plánů a příspěvky zaměstnavatele",
            "Možnosti po odchodu ze společnosti",
            "Politiky dovolené a dalších forem volna ve společnosti Faison",
            "Pravidla nároku na dovolenou",
            "Speciální druhy volna",
            "Flexibilní pracovní arrangementy",
            "Programy typu wellness a zdraví",
            "Slevy a výhody pro zaměstnance",
            "Podpora školného a profesního rozvoje",
            "Firemní rodinné akce",
            "Flexibilita a podpora pracovní doby",
            "Wellness programy",
            "Flexibilní pracovní doba",
            "Práce na dálku"
        );
    }

    @Nested
    class Question {
        public static final String Q_BENEFITS = "Vypiš mi v bodech, jaké mohu čerpat zaměstnanecké benefity.";
    }

    @Nested
    class Source {
        public static final String SOURCE_GROWTH = "Rozvoj kariéry ve firmě Faison.pdf";
        public static final String SOURCE_BENEFITS = "Benefity ve firmě Faison.pdf";
        public static final String SOURCE_BALANCE = "Zajištění rovnováhy pracovního a osobního života.docx";
    }
}
