package cz.cvut.fit.gsight.it.ollamachat;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@ComponentScan("cz.cvut.fit.gsight")
public class IntegrationTestConfig {
}
