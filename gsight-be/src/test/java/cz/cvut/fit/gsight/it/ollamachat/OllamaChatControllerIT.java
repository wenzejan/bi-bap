package cz.cvut.fit.gsight.it.ollamachat;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.gsight.dto.ContextDto;
import cz.cvut.fit.gsight.dto.RequestDto;
import cz.cvut.fit.gsight.service.chat.OllamaChatService;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.ai.chat.Generation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = { IntegrationTestConfig.class })
public class OllamaChatControllerIT {

    private static final String OLLAMA_CHAT_POST = "/api/ollama/chat";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OllamaChatService ollamaChatService;

    @BeforeEach
    public void mockOllamaChatService(){
        Mockito.when(ollamaChatService.generateResponse(ArgumentMatchers.any(ContextDto.class)))
            .thenAnswer(invocation -> {
                ContextDto context = invocation.getArgument(0);
                return new ChatResponse(List.of(new Generation(context.context())));
            });
    }

    @SneakyThrows
    private void contextSearch(RequestDto request, List<String> expectedContext, List<String> expectedSources) {
        MvcResult resultActions = mockMvc.perform(MockMvcRequestBuilders.post(OLLAMA_CHAT_POST)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.contextMetadata[*].source")
                .value(Matchers.containsInAnyOrder(expectedSources.toArray())))
            .andReturn();

        String responseString = resultActions.getResponse().getContentAsString(StandardCharsets.UTF_8);
        expectedContext.forEach(expectedItem -> assertThat(responseString, containsString(expectedItem)));
    }

    @Test
    public void ok_Benefits(){
        contextSearch(
            RequestDto.from(
                TestContextHelper.Question.Q_BENEFITS),
            TestContextHelper.Requirement.REQ_BENEFITS,
            List.of(
                TestContextHelper.Source.SOURCE_BALANCE,
                TestContextHelper.Source.SOURCE_BENEFITS,
                TestContextHelper.Source.SOURCE_GROWTH
            ));
    }
}
