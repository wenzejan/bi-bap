#!/bin/bash

while [ ! -f /tmp/ollama_ready ]; do
  echo "Waiting for Ollama to be ready..."
  sleep 5
done

echo "Ollama is ready. Starting gsight-be application..."

java $JAVA_OPTS -jar app.jar